export interface IPokemon {
  name: string;
  url: string;
  id: number;
  image: string;
}

export interface IPokemonResponse {
  count: number;
  next: string;
  previous: string;
  results: IPokemon[];
}
