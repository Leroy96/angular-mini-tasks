import { PodcastsComponent } from './components/podcasts/podcasts.component';
import { AuthorPage } from './pages/author/author.page';
import { PokemonPage } from './pages/pokemon/pokemon.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/pokemon',
  },
  {
    path: 'pokemon',
    component: PokemonPage,
  },
  {
    path: 'podcasts',
    component: PodcastsComponent,
  },
  {
    path: 'author',
    component: AuthorPage,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
