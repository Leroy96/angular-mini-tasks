import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonPage } from './pages/pokemon/pokemon.page';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { PodcastsComponent } from './components/podcasts/podcasts.component';

import { AuthorComponent } from './components/author/author.component';
import { AuthorPage } from './pages/author/author.page';

@NgModule({
  declarations: [
    AppComponent,
    AuthorComponent,
    AuthorPage,
    PokemonComponent,
    PokemonPage,
    PodcastsComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
