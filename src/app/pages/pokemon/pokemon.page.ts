import { IPokemon } from '../../models/pokemon.model';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { IPokemonResponse } from 'src/app/models/pokemon.model';

const BASE_URL = 'https://pokeapi.co/api/v2/pokemon';
const IMAGE_URL =
  'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon';

@Component({
  selector: 'app-pokemon-page',
  templateUrl: './pokemon.page.html',
  styleUrls: ['./pokemon.page.scss'],
})
export class PokemonPage implements OnInit {
  pokemons: IPokemon[] = [];

  private getImageUrl(url: string): string {
    const id = Number(url.split('/').filter(Boolean).pop());
    return `${IMAGE_URL}/${id}.png`;
  }

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.fetchPokemon();
  }

  private fetchPokemon(): void {
    this.http.get<IPokemonResponse>(BASE_URL).subscribe({
      next: (response: IPokemonResponse) => {
        console.log(response.results);
        this.pokemons = response.results.map((pokemon) => {
          return {
            ...pokemon,
            image: this.getImageUrl(pokemon.url),
          };
        });
      },
      error: () => {},
      complete: () => {},
    });
  }
}
