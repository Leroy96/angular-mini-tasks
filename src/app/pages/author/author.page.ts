import { Component } from '@angular/core';

interface IAuthor {
  name: string;
  githubLink: string;
  avatar: string;
}

@Component({
  selector: 'app-author-page',
  templateUrl: './author.page.html',
  styleUrls: ['./author.page.scss'],
})
export class AuthorPage {
  author: IAuthor = {
    name: 'Leroy Bemelen',
    githubLink: 'https://github.com/Leroy96',
    avatar: 'assets/avatar.jpg',
  };
  constructor() {}
}
