import { Component, Input, OnInit } from '@angular/core';
import { IPokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss'],
})
export class PokemonComponent implements OnInit {
  @Input() pokemons: IPokemon[] = [];

  constructor() {}

  ngOnInit(): void {}
}
