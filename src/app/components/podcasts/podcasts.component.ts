import { Component } from '@angular/core';

@Component({
  selector: 'app-podcasts',
  templateUrl: './podcasts.component.html',
  styleUrls: ['./podcasts.component.scss'],
})
export class PodcastsComponent {
  appName: string = 'Podcasts';
  appVersion: number = 1.0;
  podcastCategories: Array<any> = [
    'Gaming',
    'Minecraft',
    'StarWars',
    'Pizzas',
    'Dogs',
    'Extreme Frisbee',
  ];
}
